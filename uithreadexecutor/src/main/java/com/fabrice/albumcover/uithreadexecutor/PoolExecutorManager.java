package com.fabrice.albumcover.uithreadexecutor;

public class PoolExecutorManager {
    private static PoolExecutorManager instance;
    private ThreadExecutor threadExecutor;

    private PoolExecutorManager() {
        this.threadExecutor = new ThreadExecutor();
    }

    public static PoolExecutorManager getInstance() {
        if (instance == null) {
            instance = new PoolExecutorManager();
        }
        return instance;
    }

    public ThreadExecutor getThreadExecutor() {
        return threadExecutor;
    }
}
