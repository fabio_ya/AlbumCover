package com.fabrice.albumcover.exception;

public class AlbumException extends Exception {

    String exceptionCode;
    String message;

    public AlbumException(final String exceptionCode, final String message) {
        this.exceptionCode = exceptionCode;
        this.message = message;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionMessage() {
        return message;
    }


}
