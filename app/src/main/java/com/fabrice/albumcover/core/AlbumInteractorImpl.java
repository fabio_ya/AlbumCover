package com.fabrice.albumcover.core;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.fabrice.albumcover.exception.AlbumException;
import com.fabrice.albumcover.model.AlbumList;
import com.fabrice.albumcover.presentation.AlbumPresenter;
import com.fabrice.albumcover.repository.AlbumRepository;

import java.util.concurrent.CancellationException;

/**
 * Created by fabrice
 */

public class AlbumInteractorImpl implements AlbumInteractor {

    AlbumPresenter albumPresenter;
    AlbumRepository albumRepository;

    public AlbumInteractorImpl(final AlbumPresenter albumPresenter,
                               final AlbumRepository albumRepository) {
        this.albumPresenter = albumPresenter;
        this.albumRepository = albumRepository;
    }

    @Override
    public void fetchUserAlbums(final Context context, final String id) {
        new Handler(Looper.getMainLooper()).post(() -> {
            Log.d("UI thread", "I am the UI thread");

            try {
                AlbumList albumList = albumRepository.fetchUserAlbums(context, id);
                if (albumList == null || albumList.data.isEmpty()) {
                    albumPresenter.presentNothing();
                } else {
                    albumPresenter.presentAlbums(albumList);
                }
            } catch (CancellationException e) {
                albumPresenter.presentError();
            } catch (AlbumException e) {
                albumPresenter.presentNothing();
            }
        });
    }
}
