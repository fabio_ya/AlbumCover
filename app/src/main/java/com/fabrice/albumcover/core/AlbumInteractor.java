package com.fabrice.albumcover.core;

import android.content.Context;

import com.fabrice.albumcover.model.AlbumList;

import java.util.List;

/**
 * Created by fabrice on 25/10/2017.
 */

public interface AlbumInteractor {
    void fetchUserAlbums(final Context context, final String id);
}
