package com.fabrice.albumcover;

import com.fabrice.albumcover.core.AlbumInteractor;
import com.fabrice.albumcover.core.AlbumInteractorImpl;
import com.fabrice.albumcover.presentation.AlbumPresenter;
import com.fabrice.albumcover.presentation.AlbumPresenterImpl;
import com.fabrice.albumcover.presentation.AlbumView;
import com.fabrice.albumcover.repository.AlbumRepository;
import com.fabrice.albumcover.uithreadexecutor.PoolExecutorManager;
import com.fabrice.albumcover.uithreadexecutor.UiThreadExecutor;
import com.nicolasmouchel.executordecorator.ImmutableExecutorDecorator;
import com.nicolasmouchel.executordecorator.MutableExecutorDecorator;

/**
 * Created by fabrice
 */

public class AlbumModule {
    final private AlbumInteractor interactor;
    final private AlbumView view;

    public AlbumModule(AlbumRepository albumRepository) {
        view = provideView();
        interactor = provideInteractor(albumRepository, view);
    }

    public AlbumInteractor getInteractor() {
        return interactor;
    }

    public AlbumViewDecorator getViewDecorator() {
        return (AlbumViewDecorator) view;
    }

    @ImmutableExecutorDecorator
    private AlbumInteractor provideInteractor(AlbumRepository albumRepository, AlbumView albumView) {
        final AlbumPresenter albumPresenter = new AlbumPresenterImpl(albumView);
        final AlbumInteractorImpl albumInteractor = new AlbumInteractorImpl(albumPresenter,
                                                                            albumRepository);
        return new AlbumInteractorDecorator(PoolExecutorManager.getInstance().getThreadExecutor(),
                                            albumInteractor);
    }

    @MutableExecutorDecorator()
    private AlbumView provideView() {
        return new AlbumViewDecorator(new UiThreadExecutor()).asDecorated();
    }
}
