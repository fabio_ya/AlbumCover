package com.fabrice.albumcover.presentation;

import com.fabrice.albumcover.model.Album;
import com.fabrice.albumcover.model.AlbumList;
import com.fabrice.albumcover.model.AlbumListViewModel;
import com.fabrice.albumcover.model.AlbumViewModel;

import java.util.List;

/**
 * Created by fabrice
 */

public class AlbumPresenterImpl implements AlbumPresenter {
    AlbumView view;

    public AlbumPresenterImpl(final AlbumView view) {
        this.view = view;
    }

    @Override
    public void presentError() {
        view.presentError();
    }

    @Override
    public void presentNothing() {
        view.presentNothing();
    }

    @Override
    public void presentAlbums(final AlbumList data) {
        AlbumListViewModel albumListViewModel = new AlbumListViewModel();
        final List<Album> albumData = data.data;
        for(int i = 0; i < albumData.size()-1; i++ ){
            albumListViewModel.albumViewModelList.add(new AlbumViewModel(albumData.get(i).coverBig, albumData.get(i).artist.pictureBig));
        }
        view.presentAlbums(albumListViewModel);
    }
}
