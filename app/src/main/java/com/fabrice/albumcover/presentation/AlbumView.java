package com.fabrice.albumcover.presentation;

import com.fabrice.albumcover.model.AlbumListViewModel;

/**
 * Created by fabrice
 */

public interface AlbumView {
    void presentError();
    void presentNothing();
    void presentAlbums(AlbumListViewModel data);
}
