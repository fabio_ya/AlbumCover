package com.fabrice.albumcover.presentation;

import com.fabrice.albumcover.model.AlbumList;

/**
 * Created by fabrice
 */

public interface AlbumPresenter {
    void presentError();
    void presentNothing();
    void presentAlbums(AlbumList data);
}
