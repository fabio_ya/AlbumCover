package com.fabrice.albumcover.network.Adapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by fabrice
 */
public class Adapters {

    public static <I> I convert(SelfConvert<I> object) {
        if (object == null) {
            return null;
        }
        return object.convert();
    }

    public static <I, O> I from(O subValue, Convert<O, I> convert) throws JSONException {
        if (subValue == null) {
            return null;
        }
        return convert.from(subValue);
    }

    public static <T, U> Iterable<T> fromArray(U[] subValue, Convert<U, T> convert) {
        return new IterableAdapter(Arrays.asList(subValue), convert);
    }

    public static <T, U> Iterable<T> fromList(List<U> subValue, Convert<U, T> convert) {
        return new IterableAdapter(subValue, convert);
    }

    public static <T, U> List<U> fromIterable(Iterable<T> subModel, Convert<T, U> convert) throws JSONException {
        if (subModel == null) {
            return Collections.EMPTY_LIST;
        }
        Iterator<T> iterator = subModel.iterator();
        List<U> listResult = new ArrayList<>();
        while(iterator.hasNext()) {
            listResult.add(from(iterator.next(), convert));
        }
        return listResult;
    }

    public interface Convert<T, U> {
        U from(T object) throws JSONException;
    }

    public interface SelfConvert<U> {
        U convert();
    }

    private static class IterableAdapter<I, O> implements Iterable<I> {

        private final Iterable<O> mAdaptedObjects;
        private final Convert<O, I> mConverter;

        public IterableAdapter(Iterable<O> adaptedObjects, Convert<O, I> converter) {
            mAdaptedObjects = adaptedObjects;
            mConverter = converter;
        }

        @Override
        public Iterator<I> iterator() {
            return new ConvertIterator();
        }

        private class ConvertIterator implements Iterator<I> {

            private final Iterator<O> iterator;

            public ConvertIterator() {
                iterator = mAdaptedObjects.iterator();
            }

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public I next() {
                try {
                    return mConverter.from(iterator.next());
                } catch (JSONException e) {

                }
                return null;
            }

            @Override
            public void remove() {
                iterator.remove();
            }
        }
    }

}
