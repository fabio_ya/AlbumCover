package com.fabrice.albumcover.network.constant;

public final class WebServiceConstants {

    public static final String BASE_URL = "http://api.deezer.com";

    public static final String VERSION_API = "/%1$s";

    public static final String LIST_ALBUMS = "/user/%1$s/albums";

}
