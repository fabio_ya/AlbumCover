package com.fabrice.albumcover.network.asynctask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.fabrice.albumcover.exception.AlbumException;
import com.fabrice.albumcover.model.AlbumList;
import com.fabrice.albumcover.network.CoverAlbumApi;
import com.fabrice.albumcover.screen.helper.ConnectionHelper;

import org.json.JSONObject;


/**
 * Created by fabrice
 */

public class AlbumDownloadTask extends AsyncTask<String, Integer, AlbumList> {

    private Context mContext;

    public AlbumDownloadTask(Context context) {
        mContext = context;
    }

    /**
     * Cancel background network operation if we do not have network connectivity.
     */
    @Override
    protected void onPreExecute() {
        NetworkInfo networkInfo = ConnectionHelper.getNetworkInfo(mContext);
            if (networkInfo == null || !networkInfo.isConnected() ||
                    (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                            && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
    }

    /**
     * Defines work to perform on the background thread.
     */
    @Override
    protected AlbumList doInBackground(String... urls) {
        AlbumList result = null;
        if (!isCancelled() && urls != null && urls.length > 0) {
            String urlString = urls[0];
            try {
                JSONObject json = CoverAlbumApi.downloadUrl(urlString);
                if (json != null) {
                    result = new AlbumList.CreateFromAlbumList().from(json);
                    Log.e("contenu response",result.toString());
                } else {
                    throw new AlbumException("ERR_INT","no response received");
                }
            } catch(Exception e) {
                Log.e("error dans la response",e.getMessage());
            }
        }
        return result;
    }

    @Override
    protected void onCancelled(final AlbumList albumList) {
        super.onCancelled(albumList);
    }
}

