package com.fabrice.albumcover.model;

import com.fabrice.albumcover.network.Adapter.Adapters;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by fabrice
 */

public class AlbumList {

    public List<Album> data;
    public String checksum;
    public int total;
    public String next;


    public static class CreateFromAlbumList implements Adapters.Convert<JSONObject, AlbumList > {

        @Override
        public AlbumList from(final JSONObject object) throws JSONException {
            AlbumList albumList = new AlbumList();
            albumList.checksum = object.getString("checksum");
            albumList.total = object.getInt("total");
            albumList.next = object.getString("next");
            albumList.data = new Album.CreateFromAlbum().from(object.getJSONArray("data"));
            return albumList;
        }
    }

    @Override
    public String toString() {
        return "AlbumList{" + "data=" + data + ", checksum='" + checksum + '\'' + ", total=" + total + ", next='" + next + '\'' + '}';
    }
}
