package com.fabrice.albumcover.model;

import com.fabrice.albumcover.network.Adapter.Adapters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabrice
 */

public class Album {
    public int id;
    public String title;
    public String link;
    public String cover;
    public String coverSmall;
    public String coverMedium;
    public String coverBig;
    public String coverXl;
    public int nbTracks;
    public String releaseDate;
    public String recordType;
    public boolean available;
    public String tracklist;
    public boolean explicitLyrics;
    public int timeAdd;
    public Artist artist;
    public String type;


    @Override
    public String toString() {
        return "Album{" + "id=" + id + ", title='" + title + '\'' + ", link='" + link + '\'' + ", cover='" + cover + '\'' + ", coverSmall='" + coverSmall + '\'' + ", coverMedium='" + coverMedium + '\'' + ", coverBig='" + coverBig + '\'' + ", coverXl='" + coverXl + '\'' + ", nbTracks=" + nbTracks + ", releaseDate='" + releaseDate + '\'' + ", recordType='" + recordType + '\'' + ", available=" + available + ", tracklist='" + tracklist + '\'' + ", explicitLyrics=" + explicitLyrics + ", timeAdd=" + timeAdd + ", artist=" + artist + ", type='" + type + '\'' + '}';
    }

    public static class CreateFromAlbum implements Adapters.Convert<JSONArray, List<Album>> {
        @Override
        public List<Album> from(final JSONArray objectArray) throws JSONException {
            List<Album> albumList = new ArrayList<>();
            for (int i = 0; i < objectArray.length(); i++) {
                Album album = new Album();
                JSONObject object = objectArray.getJSONObject(i);
                album.id = object.getInt("id");
                album.title = object.getString("title");
                album.link = object.getString("link");
                album.cover = object.getString("cover");
                album.coverSmall = object.getString("cover_small");
                album.coverMedium = object.getString("cover_medium");
                album.coverBig = object.getString("cover_big");
                album.coverXl = object.getString("cover_xl");
                album.nbTracks = object.getInt("nb_tracks");
                album.artist = new Artist.CreateFromArtist().from(object.getJSONObject("artist"));
                albumList.add(album);
            }
            return albumList;
        }
    }
}
