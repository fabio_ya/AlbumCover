package com.fabrice.albumcover.model;

import java.io.Serializable;

/**
 * Created by fabrice
 */

public class AlbumViewModel implements Serializable {

    public String coverUrlImg;
    public String artistUrlImg;

    public AlbumViewModel(final String coverUrlImg, final String artistUrlImg) {
        this.coverUrlImg = coverUrlImg;
        this.artistUrlImg = artistUrlImg;
    }

    @Override
    public String toString() {
        return "AlbumViewModel{" + "coverUrlImg='" + coverUrlImg + '\'' + ", artistUrlImg='" + artistUrlImg + '\'' + '}';
    }
}
