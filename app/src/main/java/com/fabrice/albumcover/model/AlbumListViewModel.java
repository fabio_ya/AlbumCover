package com.fabrice.albumcover.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabrice
 */

public class AlbumListViewModel implements Serializable {
    public List<AlbumViewModel> albumViewModelList = new ArrayList<>();

    @Override
    public String toString() {
        return "AlbumListViewModel{" + "albumViewModelList=" + albumViewModelList + '}';
    }
}
