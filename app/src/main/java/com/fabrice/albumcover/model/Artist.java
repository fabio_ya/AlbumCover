package com.fabrice.albumcover.model;

import com.fabrice.albumcover.network.Adapter.Adapters;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by fabrice
 */

public class Artist {

    public int id;
    public String name;
    public String picture;
    public String pictureSmall;
    public String pictureMedium;
    public String pictureBig;
    public String pictureXl;
    public String tracklist;
    public String type;

    @Override
    public String toString() {
        return "Artist{" + "id=" + id + ", name='" + name + '\'' + ", picture='" + picture + '\'' + ", pictureSmall='" + pictureSmall + '\'' + ", pictureMedium='" + pictureMedium + '\'' + ", pictureBig='" + pictureBig + '\'' + ", pictureXl='" + pictureXl + '\'' + ", tracklist='" + tracklist + '\'' + ", type='" + type + '\'' + '}';
    }

    public static class CreateFromArtist implements Adapters.Convert<JSONObject, Artist> {
        @Override
        public Artist from(final JSONObject object) throws JSONException {
            Artist artist = new Artist();
            artist.id = object.getInt("id");
            artist.name = object.getString("name");
            artist.picture = object.getString("picture");
            artist.pictureSmall = object.getString("picture_small");
            artist.pictureMedium = object.getString("picture_medium");
            artist.pictureBig = object.getString("picture_big");
            return artist;
        }
    }
}
