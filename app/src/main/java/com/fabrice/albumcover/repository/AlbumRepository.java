package com.fabrice.albumcover.repository;

import android.content.Context;

import com.fabrice.albumcover.exception.AlbumException;
import com.fabrice.albumcover.model.AlbumList;

import java.util.List;

/**
 * Created by fabrice
 */

public interface AlbumRepository {
     AlbumList fetchUserAlbums(final Context context, final String id) throws AlbumException;
}
