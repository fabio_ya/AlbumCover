package com.fabrice.albumcover.repository;

import android.content.Context;
import android.util.Log;

import com.fabrice.albumcover.exception.AlbumException;
import com.fabrice.albumcover.model.AlbumList;
import com.fabrice.albumcover.network.asynctask.AlbumDownloadTask;
import com.fabrice.albumcover.network.constant.WebServiceConstants;

import java.util.concurrent.CancellationException;

/**
 * Created by fabrice
 */

public class AlbumRepositoryImpl implements AlbumRepository {

    @Override
    public AlbumList fetchUserAlbums(final Context context, final String id) throws AlbumException {
        AlbumList albumList;
        String url = String.format(WebServiceConstants.LIST_ALBUMS, id);
        AlbumDownloadTask task = new AlbumDownloadTask(context);
        try {
            albumList = task.execute(url).get();
            Log.e("Repository response ", albumList.toString());
        } catch (CancellationException e) {
            throw e;
        } catch (Exception e) {
            throw new AlbumException("ERR_INT", e.getMessage());
        }
        return albumList;
    }
}
