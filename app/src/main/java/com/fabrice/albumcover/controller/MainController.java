package com.fabrice.albumcover.controller;

import android.support.annotation.NonNull;

import com.fabrice.albumcover.AlbumModule;
import com.fabrice.albumcover.factory.AlbumModuleFactory;
import com.fabrice.albumcover.model.AlbumListViewModel;
import com.fabrice.albumcover.presentation.AlbumView;
import com.fabrice.albumcover.screen.activity.AlbumGridActivity;

/**
 * Created by fabrice
 */

public class MainController {

    private AlbumModule albumModule;

    public MainController() {
        this.albumModule = AlbumModuleFactory.getAlbumModule();
    }

    public AlbumModule getAlbumModule() {
        return albumModule;
    }
}
