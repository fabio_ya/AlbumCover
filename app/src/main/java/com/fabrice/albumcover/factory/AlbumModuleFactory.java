package com.fabrice.albumcover.factory;

import com.fabrice.albumcover.AlbumModule;
import com.fabrice.albumcover.repository.AlbumRepositoryImpl;

/**
 * Created by fabrice
 */

public class AlbumModuleFactory {

    public static AlbumModule getAlbumModule(){
        return new AlbumModule(new AlbumRepositoryImpl());
    }
}
