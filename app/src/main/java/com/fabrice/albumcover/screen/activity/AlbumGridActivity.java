package com.fabrice.albumcover.screen.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.fabrice.albumcover.R;
import com.fabrice.albumcover.controller.MainController;
import com.fabrice.albumcover.model.AlbumListViewModel;
import com.fabrice.albumcover.presentation.AlbumView;
import com.fabrice.albumcover.screen.fragment.AlbumGridFragment;
import com.fabrice.albumcover.screen.fragment.DefaultFragment;

/**
 * Simple FragmentActivity to hold the main {@link AlbumGridFragment} and not much else.
 */
public class AlbumGridActivity extends FragmentActivity implements AlbumView {

    private static final String DEFAULT_FRAGMENT_TAG = "default_fragment";
    private static final String ERROR_FRAGMENT_TAG = "error_fragment";
    private static final String GRID_FRAGMENT_TAG = "grid_fragment";
    public static final String USER_ID = "2529";
    private MainController mainController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mainController = new MainController();
            initializeAlbum();
        }
    }

    private void initializeAlbum() {
        mainController.getAlbumModule().getViewDecorator().mutate(this);
        mainController.getAlbumModule().getInteractor().fetchUserAlbums(this, USER_ID);
    }

    public void presentError() {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_FRAGMENT_TAG) == null) {
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content,
                   DefaultFragment.newInstance(R.mipmap.no_connection,
                                               R.string.no_connection_internet),
                   ERROR_FRAGMENT_TAG);
            ft.commit();
        }
    }

    public void presentNothing() {
        if (getSupportFragmentManager().findFragmentByTag(DEFAULT_FRAGMENT_TAG) == null) {
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content,
                   DefaultFragment.newInstance(R.mipmap.no_data, R.string.no_data),
                   DEFAULT_FRAGMENT_TAG);
            ft.commit();
        }
    }

    public void presentAlbums(final AlbumListViewModel data) {
        if (getSupportFragmentManager().findFragmentByTag(GRID_FRAGMENT_TAG) == null) {
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content,
                   AlbumGridFragment.newInstance(data.albumViewModelList),
                   GRID_FRAGMENT_TAG);
            ft.commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainController.getAlbumModule().getViewDecorator().mutate(null);
    }


}
