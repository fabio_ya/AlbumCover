package com.fabrice.albumcover.screen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fabrice.albumcover.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DefaultFragment extends Fragment {


    public static final String IMAGE_FRAGMENT = "image_fragment";
    public static final String TEXT_FRAGMENT = "text_fragment";
    private int mImage;
    private int mText;

    public static DefaultFragment newInstance(int image, int text) {
        DefaultFragment defaultFragment = new DefaultFragment();

        Bundle args = new Bundle();
        args.putInt(IMAGE_FRAGMENT, image);
        args.putInt(TEXT_FRAGMENT, text);
        defaultFragment.setArguments(args);

        return defaultFragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImage = getArguments().getInt(IMAGE_FRAGMENT);
        mText = getArguments().getInt(TEXT_FRAGMENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_default, container, false);
        ImageView imgView = view.findViewById(R.id.default_img);
        imgView.setImageResource(mImage);
        TextView txtView = view.findViewById(R.id.default_text);
        txtView.setText(mText);
        return view;
    }

}
