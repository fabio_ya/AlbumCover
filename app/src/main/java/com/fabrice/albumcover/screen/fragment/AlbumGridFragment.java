package com.fabrice.albumcover.screen.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.NetworkInfo;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.compat.BuildConfig;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.fabrice.albumcover.R;
import com.fabrice.albumcover.cache.ImageCache;
import com.fabrice.albumcover.cache.ImageFetcher;
import com.fabrice.albumcover.model.AlbumViewModel;
import com.fabrice.albumcover.screen.adapter.GridViewAdapter;
import com.fabrice.albumcover.screen.helper.ConnectionHelper;

import java.io.Serializable;
import java.util.List;

public class AlbumGridFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String TAG = "AlbumGridFragment";
    private static final String IMAGE_CACHE_DIR = "thumbs";
    private static final String ALBUM_VIEW_MODEL = "albumViewModel";


    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private GridViewAdapter mAdapter;
    private ImageFetcher mImageFetcher;
    private List<AlbumViewModel> mAlbumViewModelList;

    public static AlbumGridFragment newInstance(List<AlbumViewModel> albumViewModels) {
        AlbumGridFragment albumGridFragment = new AlbumGridFragment();

        Bundle args = new Bundle();
        args.putSerializable(ALBUM_VIEW_MODEL, (Serializable) albumViewModels);
        albumGridFragment.setArguments(args);

        return albumGridFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAlbumViewModelList = (List<AlbumViewModel>) getArguments().getSerializable(ALBUM_VIEW_MODEL);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(getActivity(),
                                                                                  IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.4f); // Set memory cache to 40% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getActivity(), mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);

        mAdapter = new GridViewAdapter(getActivity(), mAlbumViewModelList, mImageFetcher);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.image_grid_fragment, container, false);
        final GridView mGridView = v.findViewById(R.id.gridView);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    mImageFetcher.setPauseWork(true);
                } else {
                    mImageFetcher.setPauseWork(false);
                }
            }

            @Override
            public void onScroll(AbsListView absListView,
                                 int firstVisibleItem,
                                 int visibleItemCount,
                                 int totalItemCount) {
            }
        });

        mGridView.getViewTreeObserver()
                 .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                     @TargetApi(VERSION_CODES.JELLY_BEAN)
                     @Override
                     public void onGlobalLayout() {
                         if (mAdapter.getNumColumns() == 0) {
                             final int numColumns = (int) Math.floor(mGridView.getWidth() / (mImageThumbSize + mImageThumbSpacing));
                             if (numColumns > 0) {
                                 final int columnWidth = (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                                 mAdapter.setNumColumns(numColumns);
                                 mAdapter.setItemHeight(columnWidth);
                                 if (BuildConfig.DEBUG) {
                                     Log.d(TAG, "onCreateView - numColumns set to " + numColumns);
                                 }
                                 mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                             }
                         }
                     }
                 });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_cache:
                mImageFetcher.clearCache();
                Toast.makeText(getActivity(),
                               R.string.clear_cache_complete_toast,
                               Toast.LENGTH_SHORT).show();
                getActivity().recreate();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Context context = getContext();
        final NetworkInfo networkInfo = ConnectionHelper.getNetworkInfo(context);
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            Toast.makeText(context, R.string.no_connection_internet, Toast.LENGTH_LONG).show();
            Log.e(TAG, "checkConnection - no connection found");
        }
        if (v.getTag() == GridViewAdapter.ARTIST_TAG) {
            mImageFetcher.loadImage(((AlbumViewModel) parent.getAdapter()
                                                            .getItem(position)).coverUrlImg,
                                    (ImageView) v);
            v.setTag(GridViewAdapter.ALBUM_TAG);
        } else {
            mImageFetcher.loadImage(((AlbumViewModel) parent.getAdapter()
                                                            .getItem(position)).artistUrlImg,
                                    (ImageView) v);
            v.setTag(GridViewAdapter.ARTIST_TAG);
        }
    }
}
