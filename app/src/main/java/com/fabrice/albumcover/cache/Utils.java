package com.fabrice.albumcover.cache;


import android.os.Build;
import android.os.Build.VERSION_CODES;

public class Utils {

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;
    }
}
